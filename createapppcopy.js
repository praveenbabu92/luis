var request = require('request');
var subscriptionkey = 'db807ffad58d46f087efebbd0a94ae22';
var endpoint = 'https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/';

var config = {
    "name": 'TestApp',
    "culture": "en-us"
};
var options = {
    url: endpoint,
    headers: {
        'Ocp-Apim-Subscription-Key': subscriptionkey,
    },
    body: {
        "name": config.name,
        "culture": config.culture

    }
};
options.body = JSON.stringify(options.body);
request.post(options, function (error, response, body) {
    console.log('response is :)' + JSON.stringify(response));

});