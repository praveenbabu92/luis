var rp = require('request-promise');
var fse = require('fs-extra');
var path = require('path');

// main function to call
// Call Apps_Create
var config = {
    "name": 'TestApp',
    "culture": "en-us"
};
var createApp = async (config) => {
    try {
        // JSON for the request body
        var jsonBody = {
            "name": config.name,
            "culture": config.culture
        };

        // Create a LUIS app
        var createAppPromise = callCreateApp({
            uri: 'https://westus.api.cognitive.microsoft.com/luis/api/v2.0/apps/',
            method: 'POST',
            headers: {
                'Ocp-Apim-Subscription-Key': 'db807ffad58d46f087efebbd0a94ae22'
            },
            json: true,
            body: jsonBody
        });

        let results = await createAppPromise;

        // Create app returns an app ID
        let appId = results.response;
        console.log('final resp is :)' + JSON.stringify(results));
        console.log(`Called createApp, created app with ID ${appId}`);
        return appId;


    } catch (err) {
        console.log(`Error creating app:  ${err.message} `);
        throw err;
    }

}

// Send JSON as the body of the POST request to the API
var callCreateApp = async (options) => {
    try {
        console.log('option are :)' + JSON.stringify(options));
        var response;
        if (options.method === 'POST') {
            response = await rp.post(options);
        } else if (options.method === 'GET') { // TODO: There's no GET for create app
            response = await rp.get(options);
        }
        // response from successful create should be the new app ID
        return {
            response
        };

    } catch (err) {
        throw err;
    }
}

createApp(config);
// module.exports = createApp;