// require('dotenv').config();

var request = require('request');
var querystring = require('querystring');

function getLuisIntent(utterance) {
    var region = 'westus';
    var version = 'v2.0';
    var endpoint = `https://${region}.api.cognitive.microsoft.com/luis/${version}/apps/`;
    var luisAppId = '7429e8d4-55a9-40c1-b029-3a6ba31b6269';
    var subscriptionkey = 'db807ffad58d46f087efebbd0a94ae22';

    // Create query string 
    var queryParams = {
        "verbose": true,
        "q": utterance,
        "subscription-key": subscriptionkey

    }

    // append query string to endpoint URL
    var luisRequest = endpoint + luisAppId + '?' + querystring.stringify(queryParams);

    // HTTP Request
    request(luisRequest, function (err, response, body) {

        // HTTP Response
        if (err) {
            console.log(err);
        } else {
            console.log('body is :)' + body);
            var data = JSON.parse(body);
            console.log('Query' + data.query);
            console.log(`Top Intent: ${data.topScoringIntent.intent}`);
            console.log('Intents:');
            console.log(JSON.stringify(data.intents));
        }
    });
}

// Pass an utterance to the sample LUIS app
getLuisIntent('I want to watch a movie');