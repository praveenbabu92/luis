var request = require('request');
var express = require('express');
var querystring = require('querystring');
var app = express();
app.listen(3000);
console.log('server is running on 3000 port');

var count = 1;
var sessionObj = {
   'bot_id': '',
   'bot_name': '',
   'user_id': '',
   'intent_name': '',
   'intent_entities': [],
   'intent_fulfillment': []
};
var dbObj = {
   // "user_id": "123456",
   "bot_id": "bot_1239021812",
   "bot_name": "Book_order_cab_pizza",
   "bot_alias": "Book_order_cab_pizza",
   "bot_organisation_id": "org_123456",
   "bot_organisation_name": "Abbott",
   "bot_department_id": "dep_123456",
   "bot_department_name": "HR",
   "bot_default_answer": "Hi! I am Abbi",
   "bot_skills": [{
         "skill_id": "skill_12312098v4",
         "skill_name": "Small_Talk",
         "skill_status": "active"
      },
      {
         "skill_id": "skill_23409832v3",
         "skill_name": "Sales",
         "skill_status": "active"
      }
   ],
   "bot_intents": [{
         "intent_name": "Book a cab",
         "intent_fulfillment": ["starting point", "end point", "builtin.datetimeV2.date"],
         "intent_default_response": {
            "response_type": "text",
            "system_config": "endpoint_14563",
            "buttons_config": null,
            "table_config": null,
            "image_config": null,
            "response_prompt": "Thank you ! your leave application is submitted",
            "response_error_prompt": "There is problem with your leave application."
         },
         "intent_entities": [{
               "entity_name": "starting point",
               "entity_alias": "starting point",
               "entity_required": true,
               "entity_order": 1,
               "entity_config": {
                  "response_type": "text",
                  "system_config": "endpoint_14569",
                  "buttons_config": null,
                  "table_config": null,
                  "image_config": null,
                  "response_prompt": "where is  your pick up location?",
                  "response_error_prompt": "There is problem with the no of days you entered."
               }
            },
            {
               "entity_name": "end point",
               "entity_alias": "end point",
               "entity_required": true,
               "entity_order": 2,
               "entity_config": {
                  "response_type": "text",
                  "system_config": "endpoint_14575",
                  "buttons_config": null,
                  "table_config": null,
                  "image_config": null,
                  "response_prompt": "where  is your destination?",
                  "response_error_prompt": "There is problem with the leave type you entered."
               }
            },
            {
               "entity_name": "builtin.datetimeV2.date",
               "entity_alias": "builtin.datetimeV2.date",
               "entity_required": true,
               "entity_order": 2,
               "entity_config": {
                  "response_type": "text",
                  "system_config": "endpoint_14575",
                  "buttons_config": null,
                  "table_config": null,
                  "image_config": null,
                  "response_prompt": "at what time?",
                  "response_error_prompt": "There is problem with the leave type you entered."
               }
            }
         ]
      },
      {
         "intent_name": "order pizza",
         "intent_fulfillment": ["nonveg", "veg"],
         "intent_default_response": {
            "response_type": "text",
            "system_config": "endpoint_14563",
            "buttons_config": null,
            "table_config": null,
            "image_config": null,
            "response_prompt": "Thank you ! your leave application is submitted",
            "response_error_prompt": "There is problem with your leave application."
         },
         "intent_entities": [{
               "entity_name": "nonveg",
               "entity_alias": "nonveg",
               "entity_required": true,
               "entity_order": 1,
               "entity_config": {
                  "response_type": "text",
                  "system_config": "endpoint_14569",
                  "buttons_config": null,
                  "table_config": null,
                  "image_config": null,
                  "response_prompt": "what type of  non veg pizza you want?",
                  "response_error_prompt": "There is problem with the no of days you entered."
               }
            },
            {
               "entity_name": "veg",
               "entity_alias": "veg",
               "entity_required": true,
               "entity_order": 2,
               "entity_config": {
                  "response_type": "text",
                  "system_config": "endpoint_14575",
                  "buttons_config": null,
                  "table_config": null,
                  "image_config": null,
                  "response_prompt": "what type of veg pizza you want?",
                  "response_error_prompt": "There is problem with the leave type you entered."
               }
            }
         ]
      }
   ],
   "bot_status": "Active",
   "bot_version": "v4.0",
   "bot_systems": []
};

app.get('/sendmessegtoluis', function (req, res) {
   console.log('user query is :)' + req.query.userinput + ' userid:) ' + req.query.userid);

   var luisinputObj = {
      'appId': '61875602-2cd5-452c-81ee-3e5b9c86067d',
      'subscriptionkey': 'db807ffad58d46f087efebbd0a94ae22',
      'region': 'westus',
      'version': 'v2.0',
      'endpoint': 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/',
      'utterance': req.query.userinput
   };

   sendMessageToLuis(luisinputObj, req.query.userid, function (resp) {
      console.log('final result is:)' + resp);
      res.send(resp);
   });
});

function sendMessageToLuis(inputObj, userid, callback) {
   try {
      var intentname;
      var luis_entities = [];

      console.log('input Obj :' + JSON.stringify(inputObj));
      var queryParams = {
         "verbose": true,
         "q": inputObj.utterance,
         "subscription-key": inputObj.subscriptionkey
      };
      // append query string to endpoint URL
      var luisRequest = inputObj.endpoint + inputObj.appId + '?' + querystring.stringify(queryParams);
      // HTTP Request
      request(luisRequest, function (err, response, body) {
         if (err) {
            console.log(err);
         } else {
            console.log('body is :)' + body);
            var data = JSON.parse(body);
            var entities = data.entities;
            intentname = data.topScoringIntent.intent;

            if (intentname != null && intentname == 'None') {
               emptySessionObject();
               return callback('I did not get it right. Can you please rephrase? ');
            } else {
               // appending intent name to session object
               if (entities != null && entities.length > 0) {
                  for (i = 0; i <= entities.length - 1; i++) {
                     var entitymap = {};
                     entitymap["entity_name"] = entities[i].type;
                     entitymap["entity_value"] = entities[i].entity;
                     luis_entities.push(entitymap);
                  }
               } else {
                  console.log('!!!!!!!!!!!!!!!!');
                  var entityprompt = getPromptForFirstEntity(dbObj, intentname);
                  getDataFromDataBase(sessionObj, userid, dbObj, intentname);
                  updateSessionObject(sessionObj, luis_entities, intentname, userid, dbObj.bot_id, dbObj.bot_name, function (callback1) {
                     console.log('$$$$$$$$$$$$$$$$$$$$' + JSON.stringify(callback1));
                  });
                  if (userid == sessionObj.user_id && dbObj.bot_id == sessionObj.bot_id && intentname != sessionObj.intent_name) {
                     console.log(' if entities are empty then userid is ::::' + userid + ' sesson object userid is :) ' + sessionObj.user_id);
                     emptySessionObject();
                     console.log('%%%%%%%%' + JSON.stringify(sessionObj));
                  } else {
                     sessionObj = sessionObj;
                  }
                  return callback(entityprompt);
               }
            }
            // Intent switching
            console.log('Before @@@@@@@ ******' + JSON.stringify(sessionObj));
            if (userid == sessionObj.user_id && dbObj.bot_id == sessionObj.bot_id && intentname != sessionObj.intent_name) {
               console.log('userid is ::::' + userid + ' sesson object userid is :) ' + sessionObj.user_id);
               emptySessionObject();
               getDataFromDataBase(sessionObj, userid, dbObj, intentname);
            } else {
               console.log('**********************');
               getDataFromDataBase(sessionObj, userid, dbObj, intentname);
            }

            console.log('Before @@@@@@@' + JSON.stringify(sessionObj));
            updateSessionObject(sessionObj, luis_entities, intentname, userid, dbObj.bot_id, dbObj.bot_name, function (callback1) {
               sessionObj.intent_fulfillment = callback1.intent_fulfillment;
               var updatedfullfillvalues = sessionObj.intent_fulfillment;
               if (updatedfullfillvalues.length > 0) {
                  for (i = 0; i <= sessionObj.intent_entities.length - 1; i++) {
                     if (updatedfullfillvalues[0] == sessionObj.intent_entities[i].entity_name) {
                        return callback(sessionObj.intent_entities[i].entity_config.response_prompt);
                     }
                  }
               } else {
                  emptySessionObject(sessionObj);
                  console.log('before fufilling the request ' + JSON.stringify(sessionObj));
                  // console.log('afetr fufilling the dbObject is ' + JSON.stringify(dbObj.bot_intents));
                  return callback('Your intent is ready for fullfillment');
               }
            });
         }
      });
   } catch (error) {
      console.log('Error in sendMessageToLuis :)' + error);
   }
}

// elicit prompt if luis entities are empty
function getPromptForFirstEntity(obj, currentintentname) {
   var db_intents = obj.bot_intents;
   for (i = 0; i <= db_intents.length - 1; i++) {
      if (db_intents[i].intent_entities.length != null && db_intents[i].intent_entities.length > 0 && db_intents[i].intent_name == currentintentname) {
         for (j = 0; j <= db_intents[i].intent_entities.length - 1; j++) {
            return db_intents[i].intent_entities[j].entity_config.response_prompt;
         }
      }
   }
}

// update sessionobject
function updateSessionObject(sObj, luisentities, intentname, userid, botid, botname, callback) {
   try {
      // console.log('@sobj is :)' + JSON.stringify(sObj));
      var keys = Object.keys(luisentities);
      var fulfillmententities = sObj.intent_fulfillment;
      console.log('before::' + JSON.stringify(fulfillmententities));
      if (sObj !== null && sObj !== undefined) {
         if (sObj.user_id == userid && sObj.bot_id == botid && sObj.intent_name == intentname && sObj.bot_name == botname) {
            console.log('intent name is :) ' + intentname + ' session object intent name is :)' + sObj.intent_name);
            if (fulfillmententities.length > 0) {
               for (i = 0; i <= keys.length - 1; i++) {
                  if (luisentities.length > 0) {
                     for (j = 0; j <= fulfillmententities.length - 1; j++) {
                        if (fulfillmententities[j] == luisentities[keys[i]].entity_name) {
                           // remove the current element from the fullfillment array
                           fulfillmententities.splice(j, 1);
                           // here update the new fullfillment array to sObj
                           console.log('after :::' + JSON.stringify(fulfillmententities));
                           sObj.intent_fulfillment = fulfillmententities;
                        }
                     }
                  } else {
                     sObj.intent_fulfillment = fulfillmententities;
                  }
               }
            }
            callback(sObj);
         }
      }
   } catch (error) {
      console.log('Error in update session object :)' + error);
   }
}

// function to get data from database
function getDataFromDataBase(sessionObj, userid, dbObj, intentname) {
   try {
      console.log(' intent name is :) ' + intentname);
      var intentfulfillment;
      if (sessionObj.intent_name == '') {
         sessionObj.bot_id = dbObj.bot_id;
         sessionObj.bot_name = dbObj.bot_name;
         sessionObj.intent_name = intentname;
         sessionObj.user_id = userid;
         console.log('((((((((((' + JSON.stringify(sessionObj));
         for (i = 0; i <= dbObj.bot_intents.length - 1; i++) {
            console.log('------------');
            if (intentname == dbObj.bot_intents[i].intent_name) {
               console.log(intentname + '=============' + dbObj.bot_intents[i].intent_name + ' $$$$$ ' + dbObj.bot_intents[i].intent_fulfillment);
               // if (dbObj.bot_intents[i].intent_fulfillment.length > 0) {
               intentfulfillment = Array.from(dbObj.bot_intents[i].intent_fulfillment);
               console.log('+++++++++++' + JSON.stringify(intentfulfillment));
               if (dbObj.bot_intents[i].intent_entities.length != null && dbObj.bot_intents[i].intent_entities.length > 0) {
                  for (j = 0; j <= dbObj.bot_intents[i].intent_entities.length - 1; j++) {
                     // add intent entities to session object here 
                     sessionObj.intent_entities.push(dbObj.bot_intents[i].intent_entities[j]);
                  }
               }
               console.log('intent fulfillment ##' + JSON.stringify(intentfulfillment));
               sessionObj.intent_fulfillment = intentfulfillment;
            }
         }
      }
   } catch (error) {
      console.log('Error in get data from database :)' + error);
   }
}

// empty the session object
function emptySessionObject() {
   sessionObj.intent_name = '';
   sessionObj.intent_entities = [];
   sessionObj.intent_fulfillment = [];
}